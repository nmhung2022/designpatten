package service;

import model.CreateUserAction;

public abstract class ActionSet {
	public void createUser(String userName) {
		CreateUserAction cmd = this.getCreataUserAction();
		cmd.setUserName(userName);
		cmd.execute();
	}

	public abstract CreateUserAction getCreataUserAction();

}

