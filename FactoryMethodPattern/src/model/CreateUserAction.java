package model;

public abstract class CreateUserAction {
	private String userName;

	public abstract void execute();
	
	public abstract void printResults(Process process);

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
