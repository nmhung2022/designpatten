package component;

public interface FileComponent {
    void hienThiThuocTinh();

    double tongKichThuoc();

    void setDuongDan(String folderParent);

    public int CountMultimediaItem() throws Exception;

}
