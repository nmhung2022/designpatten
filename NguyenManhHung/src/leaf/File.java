package leaf;

import java.util.Arrays;

import component.FileComponent;

public class File implements FileComponent {
    private String tenFile;
    private String duoiFile;
    private double dungLuong;
    private String path;

    public File(String tenFile, String duoiFile, double dungLuong) {
	super();
	this.tenFile = tenFile;
	this.duoiFile = duoiFile;
	this.dungLuong = dungLuong;
	this.path = tenFile + "." + duoiFile;
    }

    public String getTenFile() {
	return tenFile;
    }

    public void setTenFile(String tenFile) {
	this.tenFile = tenFile;
    }

    public String getDuoiFile() {
	return duoiFile;
    }

    public void setDuoiFile(String duoiFile) {
	this.duoiFile = duoiFile;
    }

    public double getDungLuong() {
	return dungLuong;
    }

    public void setDungLuong(double dungLuong) {
	this.dungLuong = dungLuong;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }

    @Override
    public void hienThiThuocTinh() {
	System.out
		.println("File = [Tên file = " + tenFile + ", Đuôi file = " + duoiFile + ", Đường Dẫn = " + path + "]");

    }

    @Override
    public double tongKichThuoc() {
	return this.dungLuong;
    }

    @Override
    public void setDuongDan(String folder) {
	path = folder + "\\" + this.getPath();

    }

    public boolean IsMultimediaItem() {
	if (this.duoiFile.equalsIgnoreCase("MP3") || this.duoiFile.equalsIgnoreCase("MP4")
		|| this.duoiFile.equalsIgnoreCase("AVI") || this.duoiFile.equalsIgnoreCase("WAV")
		|| this.duoiFile.equalsIgnoreCase("MIDI") || this.duoiFile.equalsIgnoreCase("DAT")
		|| this.duoiFile.equalsIgnoreCase("MPEG")) {
	    return true;
	}
	return false;
    }

    @Override
    public int CountMultimediaItem() {
	return 0;
    }

}
