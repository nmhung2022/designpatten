package composite;

import java.util.ArrayList;
import java.util.List;

import component.FileComponent;
import leaf.File;

public class Folder implements FileComponent {
    private String tenThuMuc;
    private String path;
    private List<FileComponent> files;

    public Folder(String tenThuMuc, String path, List<FileComponent> files) {
	super();
	this.tenThuMuc = tenThuMuc;
	this.path = path;
	this.files = files;
    }

    public Folder(String tenThuMuc) {
	super();
	this.tenThuMuc = tenThuMuc;
	this.files = new ArrayList<>();
    }

    public Folder(String tenThuMuc, List<FileComponent> files) {
	super();
	this.tenThuMuc = tenThuMuc;
	this.files = files;
    }

    public String getTenThuMuc() {
	return tenThuMuc;
    }

    public void setTenThuMuc(String tenThuMuc) {
	this.tenThuMuc = tenThuMuc;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }

    public List<FileComponent> getFiles() {
	return files;
    }

    public void setFiles(List<FileComponent> files) {
	this.files = files;
    }

    @Override
    public void hienThiThuocTinh() {
	for (FileComponent f : files) {
	    f.hienThiThuocTinh();
	}
    }

    @Override
    public double tongKichThuoc() {
	double tong = 0;
	for (FileComponent file : files) {
	    tong += file.tongKichThuoc();
	}
	return tong;
    }

    @Override
    public void setDuongDan(String folder) {
	path = folder + "\\" + this.getPath();
    }

    public void themVaoFolder(FileComponent obj) throws Exception {
	if (!files.contains(obj)) {
	    files.add(obj);
	    obj.setDuongDan(this.getPath());
	}
    }

    public int CountMultimediaFolder() throws Exception {
	int countOfFolder = 0;
	for (FileComponent f : files) {
	    if (f instanceof Folder) {
		countOfFolder += 1;
	    }
	}
	return countOfFolder;
    }

    @Override
    public int CountMultimediaItem() throws Exception {

	int countOfMultimedia = files.size();
	for (FileComponent f : files) {
	    countOfMultimedia += f.CountMultimediaItem();
	}
	return countOfMultimedia - this.CountMultimediaFolder();
    }

}
