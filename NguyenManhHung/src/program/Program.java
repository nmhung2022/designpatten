package program;

import java.util.Arrays;
import java.util.List;

import component.FileComponent;
import composite.Folder;
import leaf.File;

public class Program {
    public static void main(String[] args) {
	try {
	    File sample = new File("sample", "pptx", 4.6);
	    File tep5 = new File("Tệp 5", "wav", 10.6);
	    File thongKe = new File("Thống kê", "xlsx", 114.6);

	    Folder folder1 = new Folder("Thư mục 1");
	    File tep1 = new File("tệp 1", "lyric", 114.6);
	    File tep2 = new File("tệp 1", "mp3", 114.6);
	    File tep3 = new File("tệp 2", "avi", 114.6);
	    File tep4 = new File("tệp 4", "mp4", 114.6);

	    folder1.themVaoFolder(tep1);
	    folder1.themVaoFolder(tep2);
	    folder1.themVaoFolder(tep3);
	    folder1.themVaoFolder(tep4);

	    Folder folderConCuafolder1 = new Folder("Thư mục 1.1");
	    folder1.themVaoFolder(folderConCuafolder1);

	    File tep13 = new File("tệp 3", "mp3", 114.6);
	    File tep14 = new File("tệp 4", "txt", 114.6);
	    folderConCuafolder1.themVaoFolder(tep13);
	    folderConCuafolder1.themVaoFolder(tep14);

	    Folder folderConCuafolder2 = new Folder("Thư mục 1.2");
	    folder1.themVaoFolder(folderConCuafolder2);
	    File tep16 = new File("tệp 6", "mp3.txt", 114.6);
	    folderConCuafolder2.themVaoFolder(tep16);

	    Folder folder2 = new Folder("Thư mục 2");

	    System.out.println("Số lượng file: " + folder1.CountMultimediaItem());

	} catch (Exception e) {
	    e.printStackTrace();
	    e.getMessage();
	}
    }
}
