package devices;

// Interface cho các đối tượng nhóm B
public interface Device {
	boolean đangBật();

	void bật();

	void tắt();

	int getÂmLượng();

	void setÂmLượng(int percent);

	int getKênh();

	void setKênh(int channel);

	void inThôngTin();
}
