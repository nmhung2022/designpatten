package devices;

public class Radio implements Device {
	private boolean dangBat = false;
	private int âmLượng = 30;
	private int kênh = 1;

	@Override
	public boolean đangBật() {
		return dangBat;
	}

	@Override
	public void bật() {
		dangBat = true;
	}

	@Override
	public void tắt() {
		dangBat = false;
	}

	@Override
	public int getÂmLượng() {
		return âmLượng;
	}

	@Override
	public void setÂmLượng(int volume) {
		if (volume > 100) {
			this.âmLượng = 100;
		} else if (volume < 0) {
			this.âmLượng = 0;
		} else {
			this.âmLượng = volume;
		}
	}

	@Override
	public int getKênh() {
		return kênh;
	}

	@Override
	public void setKênh(int channel) {
		this.kênh = channel;
	}

	@Override
	public void inThôngTin() {
		System.out.println("------------------------------------");
		System.out.println("| I'm radio.");
		System.out.println("| I'm " + (dangBat ? "enabled" : "disabled"));
		System.out.println("| Current volume is " + âmLượng + "%");
		System.out.println("| Current channel is " + kênh);
		System.out.println("------------------------------------\n");
	}
}
