package program;

import devices.Device;
import devices.Tv;
import remotes.AdvancedRemote;
import remotes.BasicRemote;
import remotes.Remote;

public class Test {
    public static void main(String[] args) {
	testDevice(new Tv());
//	1) Hãy sử dụng mẫu Bridge để giải quyết trường hợp sau đây
//    - Định nghĩa lớp loại B có tên là DataSet
//    	có thuộc tính List<Entity>
//    - Định nghĩa TextDataSet
//    - Định nghĩa SQLServerDataSet
//    - Định nghĩa interface cho lớp loại A là Accessor
//    	List<Entity> search(String keyword);
//    	void insert(Entity entity);
//    	void update(Entity entity);
//    	void delete(Entity entity);
//    - Định nghĩa các Accessor cho từng loại DataSet
//    - Định nghĩa chương trình test Program
//    	Sử dụng Accessor cho từng loại DataSet cụ thể
    }

    public static void testDevice(Device thietBi) {
	System.out.println("Thí nghiệm với BasicRemote.");
	Remote remote = new BasicRemote(thietBi);

	remote.bấmNútPower();
	thietBi.inThôngTin();

	remote.tăngÂmLượng();
	thietBi.inThôngTin();

	System.out.println("Thí nghiệm với AdvRemote.");
	Remote remote2 = new AdvancedRemote(thietBi);
	remote2.bấmNútPower();
	thietBi.inThôngTin();

	remote2.bấmNútPower();
	thietBi.inThôngTin();

	((AdvancedRemote) remote2).mute();
	thietBi.inThôngTin();
    }
}
