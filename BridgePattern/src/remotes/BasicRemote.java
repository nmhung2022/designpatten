package remotes;

import devices.Device;

// Ví dụ về một cái điều khiển cơ bản
// wrapper/handler object
public class BasicRemote implements Remote {
	protected Device thietBi; // tham chiếu đến thành phần trừu tượng

	public BasicRemote() {
	}

	public BasicRemote(Device thietBi) {
		this.thietBi = thietBi;
	}

	@Override
	public void bấmNútPower() {
		// chuyển lệnh tương ứng đến chủ thể loại B
		System.out.println("CáiĐiềuKhiển (BasicRemote): bấm vào nút POWER để điều khiển thiết bị");
		if (thietBi.đangBật()) {
			thietBi.tắt();
		} else {
			thietBi.bật();
		}
	}

	@Override
	public void giảmÂmLượng() {
		// chuyển lệnh ... B
		System.out.println("CáiĐiềuKhiển: yc giảmÂmLượng cho thiết bị -10");
		thietBi.setÂmLượng(thietBi.getÂmLượng() - 10);
	}

	@Override
	public void tăngÂmLượng() {
		System.out.println("CáiĐiềuKhiển: tăng âm lượng cho thiết bị +10");
		thietBi.setÂmLượng(thietBi.getÂmLượng() + 10);
	}

	@Override
	public void giảmSốHiệuKênh() {
		System.out.println("CáiĐiềuKhiển: giảm số hiệu kênh");
		thietBi.setKênh(thietBi.getKênh() - 1);
	}

	@Override
	public void tăngSốHiệuKênh() {
		System.out.println("CáiĐiềuKhiển: tăng số hiệu kênh");
		thietBi.setKênh(thietBi.getKênh() + 1);
	}
}
