package remotes;

// Cây cầu -- các method truy xuất/xử lý Chủ thể
// Interface cho các đối tượng loại A
public interface Remote {
    void bấmNútPower();

    void giảmÂmLượng();

    void tăngÂmLượng();

    void giảmSốHiệuKênh();

    void tăngSốHiệuKênh();
}
