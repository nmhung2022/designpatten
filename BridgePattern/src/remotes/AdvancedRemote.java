package remotes;

import devices.Device;

public class AdvancedRemote extends BasicRemote {

	public AdvancedRemote(Device thietBi) {
		super.thietBi = thietBi;
	}

	public void mute() {
		System.out.println("CáiĐiềuKhiển (loại Adv): thiết lập MUTE cho thiết bị");
		thietBi.setÂmLượng(0);
	}
}
