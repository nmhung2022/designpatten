package services;

import keyboard.BanPhim;
import keyboard.BanPhimAppleHigh;
import keyboard.BanPhimAppleIntermediate;
import screen.ManHinh;
import screen.ManHinhAppleHigh;
import screen.ManHinhAppleIntermediate;

public class MayTinhAppleFactory extends MayTinhAbstractFactory {

	@Override
	public BanPhim sanXuatBanPhim() {
		// TODO Auto-generated method stub
//		Logic tạo bàn phím apple ở nhiều phân khúc khác nhau
		int min = 1;
		int max = 10;

		int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
		if (random_int > 5) {
			return new BanPhimAppleHigh();

		} else {
			return new BanPhimAppleIntermediate();
		}

	}

	@Override
	public ManHinh sanXuatManHinh() {
		// TODO Auto-generated method stub
//		Logic tạo màn hinh apple ở nhiều phân khúc khác nhau
		int min = 1;
		int max = 10;

		int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
		if (random_int > 5) {
			return new ManHinhAppleHigh();

		} else {
			return new ManHinhAppleIntermediate();
		}
	}

}
