package services;

import keyboard.BanPhim;
import screen.ManHinh;

public abstract class MayTinhAbstractFactory {
	public abstract BanPhim sanXuatBanPhim();

	public abstract ManHinh sanXuatManHinh();

}

