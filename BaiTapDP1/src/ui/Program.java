package ui;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import model.Student;
import service.FileService;
import service.Service;
import service.ServiceFactory;

public class Program {

    public static void saveToFile(List<Student> lst) {
	try {
	    FileWriter fw = new FileWriter("output.txt", true);
	    PrintWriter pw = new PrintWriter(fw);
	    for (Student s : lst) {
		pw.println(s.writeFile());
	    }
	    fw.close();
	    pw.close();
	    System.out.println("Đã lưu vào file!");
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    static List sortListByDate(List<Student> lst) {

	Comparator<Student> comparator = (s1, s2) -> {
	    Date date1 = null, date2 = null;
	    try {
		date1 = new SimpleDateFormat("dd/MM/yyyy").parse(s1.getBirthOfDate());
		date2 = new SimpleDateFormat("dd/MM/yyyy").parse(s2.getBirthOfDate());
	    } catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    return Long.valueOf(date2.getTime()).compareTo(date1.getTime());
	};
	Collections.sort(lst, comparator);
	return lst;
    }

    static List filterByName(List<Student> lst, String name) {

	List<Student> result = new ArrayList<Student>();

	for (Student s : lst) {
	    if (s.getName().toLowerCase().contains(name.toLowerCase())) {
		result.add(s);
	    }
	}
	return result;
    }

    public static void main(String[] args) {

	// TODO Auto-generated method stub
//	Service svc = ServiceFactory.getService();
//	List<Student> lst = svc.showStudent();
//
//	// In danh sách hình
//	System.out.println("Câu 1. Danh sách các sinh viên đọc được từ file \n");
//	for (Student x : lst) {
//	    System.out.println(x.getInfo());
//	}
//	System.out.println("\nCâu 2. Danh sách các sinh viên sau khi sắp xếp theo thứ tự giản dần ngày sinh \n");
//	sortListByDate(lst);
//	for (Student x : lst) {
//	    System.out.println(x.getInfo());
//	}
//	System.out.println("\nCâu 3. Danh sách các sinh viên có 1 cụm từ bất kì: "+ "Trần \n");
//	var result = filterByName(lst, "Trần");
//	for (Object x : result) {
//	    System.out.println(((Student) x).getInfo());
//	}
//	System.out.println("\n Câu 4. Danh sách các sinh viên sau khi xoá những sinh viên trước năm 1990\n");
//	removeByYear(lst, 1990);
//	for (Student x : lst) {
//	    System.out.println(x.getInfo());
//	}
//	
//	saveToFile(lst);

	Service svc = new FileService();
	svc.showStudent();

    }

}
