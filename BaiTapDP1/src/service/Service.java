package service;

import java.util.List;

import model.Student;

public interface Service {
	public List<Student> showStudent();
}
