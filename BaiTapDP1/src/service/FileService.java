package service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import model.Student;

public class FileService implements Service {

    public List<model.Student> lst = new ArrayList<>();

    @Override
    public List<model.Student> showStudent() {

	String path = "D:\\Development\\JavaCourse\\BaiTapDP1\\input.txt";

	try {
	    FileInputStream fis = new FileInputStream(path);
	    InputStreamReader isr = new InputStreamReader(fis);
	    BufferedReader br = new BufferedReader(isr);
	    while (true) {
		String st = br.readLine();
		if (st == null || st == "")
		    break;
		String ds[] = st.split(";");
		Student s = new Student(ds[0], ds[1], Boolean.parseBoolean(ds[2]), ds[3]);
		lst.add(s);

	    }
	    br.close();
	    isr.close();
	    fis.close();

	} catch (Exception e) {
	    System.out.println(e);
	}
	return lst;
    }

}
