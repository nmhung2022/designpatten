package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import model.Student;

public class DBService implements Service {
    private DBService dbIsntance;
    private  Connection con;
    private Statement stmt;

    public DBService getInstance() {
	if (dbIsntance == null) {
	    dbIsntance = new DBService();
	}
	return dbIsntance;
    }

    public Connection getConnection() {

	if (con == null) {
	    try {
		String Host = "jdbc:sqlserver://localhost:1433;databaseName=Student";
		String username = "sa";
		String password = "123";
		con = DriverManager.getConnection(Host, username, password);
	    } catch (SQLException ex) {
		ex.printStackTrace();
	    }
	}

	return con;
    }

    @Override
    public List<Student> showStudent() {
	// TODO Auto-generated method stub
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {
	    conn = getConnection();
	    stmt = conn.createStatement();

	    String sql1b = "SELECT * FROM Student";
	    rs = stmt.executeQuery(sql1b);
	    System.out.println("Câu 1. Danh sách các sinh viên đọc được từ Database \n");
	    while (rs.next()) {

		String idStudent = rs.getString("idStudent");
		String name = rs.getString("name");
		String gender = rs.getString("gender");
		Date birthOfDate = rs.getDate("birthOfDate");
		boolean sex = false;
		
		if(gender.equals("1")) sex = true;
		else sex = false;
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(birthOfDate);
		
		System.out.println(idStudent+";"+name+ ";"+sex+";"+strDate);
	    }
	    
	    
	    String sql2b = "SELECT * FROM Student Order By birthOfDate DESC";
	    rs = stmt.executeQuery(sql2b);
	    System.out.println("\nCâu 2. Danh sách các sinh viên sau khi sắp xếp theo thứ tự giản dần ngày sinh \n");
	    while (rs.next()) {

		String idStudent = rs.getString("idStudent");
		String name = rs.getString("name");
		String gender = rs.getString("gender");
		Date birthOfDate = rs.getDate("birthOfDate");
		boolean sex = true;
		if(gender.equals("1")) sex = true;
		else sex = false;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(birthOfDate);
		
		System.out.println(idStudent+";"+name+ ";"+sex+";"+strDate);
		
	    }
	    
	    String keyword = "Trần";
	    String sqlb3 = "SELECT *From Student Where name Like N'%"+ keyword +"%'";
	    rs = stmt.executeQuery(sqlb3);
	    System.out.println("\nCâu 3. Danh sách các sinh viên có 1 cụm từ bất kì: "+ "Trần \n");
	    while (rs.next()) {

		String idStudent = rs.getString("idStudent");
		String name = rs.getString("name");
		String gender = rs.getString("gender");
		Date birthOfDate = rs.getDate("birthOfDate");
		boolean sex = true;
		if(gender.equals("1")) sex = true;
		else sex = false;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(birthOfDate);
		
		System.out.println(idStudent+";"+name+ ";"+sex+";"+strDate);
		
	    }
	    
	    
	    String sqlb4 = "DELETE FROM [Student] Where YEAR(birthOfDate) <= 1900";
	    rs = stmt.executeQuery(sqlb4);
	    System.out.println("\n Câu 4. Danh sách các sinh viên sau khi xoá những sinh viên trước năm 1990\n");
	    while (rs.next()) {

		String idStudent = rs.getString("idStudent");
		String name = rs.getString("name");
		String gender = rs.getString("gender");
		Date birthOfDate = rs.getDate("birthOfDate");
		boolean sex = true;
		if(gender.equals("1")) sex = true;
		else sex = false;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(birthOfDate);
		
		System.out.println(idStudent+";"+name+ ";"+sex+";"+strDate);
		
	    }
	    
	    

	} catch (SQLException e) {
	    e.printStackTrace();

	}
	return null;
    }


}
