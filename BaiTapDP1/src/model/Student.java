package model;

public class Student {
    private String idStudent;
    private String name;
    private boolean gender;
    private String birthOfDate;
    public Student(String idStudent, String name, boolean gender, String birthOfDate) {
	super();
	this.idStudent = idStudent;
	this.name = name;
	this.gender = gender;
	this.birthOfDate = birthOfDate;
    }
    
    public String getInfo() {
	return "Student: " + idStudent + " " + name + " " + gender + " " + birthOfDate;
    }
    
    public String getIdStudent() {
        return idStudent;
    }
    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean getGender() {
        return gender;
    }
    public void setGender(boolean gender) {
        this.gender = gender;
    }
    public String getBirthOfDate() {
        return birthOfDate;
    }
    public void setBirthOfDate(String birthOfDate) {
        this.birthOfDate = birthOfDate;
    }
    public String writeFile() {
	return idStudent+";"+name+ ";"+gender+";"+birthOfDate;
    }
}
