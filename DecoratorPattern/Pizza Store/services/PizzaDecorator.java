package services;

public abstract class PizzaDecorator implements Pizza {
    protected Pizza mPizza;
    
    public PizzaDecorator(Pizza pizza) {
        mPizza = pizza;
    }
    public Pizza getPizza() {
        return mPizza;
    }

    public void setPizza(Pizza mPizza) {
        this.mPizza = mPizza;
    }
}
