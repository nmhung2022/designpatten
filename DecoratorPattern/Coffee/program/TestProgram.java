package program;

import java.util.ArrayList;
import java.util.List;

import component.Cafe;
import component.CafeRangXay;
import decorator.Rhum;
import decorator.Sữa;
import decorator.Đường;



public class TestProgram {

	public static void main(String[] args) {

		Cafe x;

//		x = new CafePhin();
		x = new CafeRangXay();
		System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

		try {
			x = new Đường(x);
			System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

			x = new Sữa(x);
			System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

			x = new Đường(x);
			System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

			x = new Rhum(x);
			System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

			x = new Đường(x);
			System.out.println("x = " + x.getMôTả() + " giá " + x.getGiá());

		} catch (Exception exc) {
			System.out.println("Lỗi khi trang trí " + exc.getMessage());
		}

		try {
			x = new Sữa(x);
		} catch (Exception exc) {
			System.out.println("Lỗi khi trang trí " + exc.getMessage());
		}

		List<Cafe> lst = new ArrayList<>();
		lst.add(x);


		for (Cafe o : lst)
			System.out.println("Cafe = " + o.getMôTả() + " giá " + o.getGiá());
	}

}
