package decorator;

import component.Cafe;

public class Đường extends Decorator {

	@Override
	public String getMôTả() {
		return this.getCafe().getMôTả() + ", đường";
	}

	@Override
	public double getGiá() {
		return this.getCafe().getGiá() + 1;
	}

	public Đường(Cafe obj) throws Exception {
		// Đếm số lớp đường đã trang trí lên đối tượng cafe!!!!
		int soLanTrangTriDuong = 0;
		Cafe x = obj;
		while (true) {
			if (x instanceof Decorator) {
				if (x instanceof Đường)
					soLanTrangTriDuong++;

				x = ((Decorator) x).getCafe();
			} else
				break;
		}

		if (soLanTrangTriDuong >= 2)
			throw new Exception("Không được tạo nhiều hơn 2 lớp Đường cho cafe");
		
		this.cafe = obj;
	}
}
