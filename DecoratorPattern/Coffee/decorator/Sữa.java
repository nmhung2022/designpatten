package decorator;

import component.Cafe;

public class Sữa extends Decorator {

	@Override
	public String getMôTả() {
		return this.getCafe().getMôTả() + ", sữa";
	}

	@Override
	public double getGiá() {
		return this.getCafe().getGiá() + 2;
	}

	public Sữa(Cafe cafe) throws Exception {
		if (đãCóSữa(cafe))
			throw new Exception("Không được thêm nhiều hơn 1 lớp sữa cho cafe");
		
		this.cafe = cafe;
	}
	
	public boolean đãCóSữa(Cafe obj) {
		Cafe x = obj;
		while (true) {
			if (x instanceof Decorator) {
				if (x instanceof Sữa)
					return true;
				else {
					x = ((Decorator)x).getCafe();
				}
			}
			else
				break;
		}
		
		return false;
	}
}
