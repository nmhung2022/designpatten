package decorator;

import component.Cafe;

public class Rhum extends Decorator {

	@Override
	public String getMôTả() {
		return this.getCafe().getMôTả() + ", rhum";
	}

	@Override
	public double getGiá() {
		return this.getCafe().getGiá() + 5;
	}

	public Rhum(Cafe o) throws Exception {
		boolean daCoDuongHoacSua = false;
		Cafe x = o;
		while (true) {
			if (x instanceof Decorator) {
				if (x instanceof Đường || x instanceof Sữa) {
					daCoDuongHoacSua = true;
					break;
				}
				x = ((Decorator) x).getCafe();
			} else
				break;
		}

		if (daCoDuongHoacSua)
			throw new Exception("Không được thêm Rhum cho cafe đã có đường hoặc sữa");

		this.cafe = o;
	}

}
