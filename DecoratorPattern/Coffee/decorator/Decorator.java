package decorator;

import component.Cafe;

public abstract class Decorator extends Cafe{
	protected Cafe cafe; // component
	
	public abstract String getMôTả();
	
	public Cafe getCafe() {
		return this.cafe;
	}
}
