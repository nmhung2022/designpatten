package example_1;
public interface IPrototype {
	/** nhân bản đối tượng ra thành đối tượng KHÁC */
	IPrototype saoChep();
}
