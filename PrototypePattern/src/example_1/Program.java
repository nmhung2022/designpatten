package example_1;

public class Program {
	public static void main(String[] args) {
		Shape o = new Shape(25, 10);
		System.out.println("O = " + o);
		
		Shape a = o.saoChep();
		System.out.println("A = " + a);
		
		Circle o2 = new Circle(1, 3, 5);
		System.out.println("O2 " + o2);
		
		Circle o3 = o2.saoChep();
		System.out.println("O3 " + o3);
	}
}
