package example_1;

public class Shape implements IPrototype {
	private int x;
	private int y;
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return String.format("(%d, %d)", this.getX(), this.getY());
	}
	
	public Shape() {
		
	}
	
	/**
	 * Khởi tạo đối tượng từ đối tượng khác có sẵn
	 * @param obj
	 */
	public Shape(Shape obj) {
		this.x = obj.x;
		this.y = obj.y;
	}
	
	public Shape(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
	
	@Override
	public Shape saoChep() {
		return new Shape(this);
	}
}
