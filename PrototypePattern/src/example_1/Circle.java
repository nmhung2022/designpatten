package example_1;

public class Circle extends Shape {
	public int getBanKinh() {
		return banKinh;
	}

	public void setBanKinh(int banKinh) {
		this.banKinh = banKinh;
	}

	private int banKinh;

	public Circle(int x, int y, int r) {
		super(x, y);

		this.setBanKinh(r);
	}

	public Circle(Circle obj) {
		this.setX(obj.getX());
		this.setY(obj.getY());
		this.setBanKinh(obj.getBanKinh());
	}

	@Override
	public Circle saoChep() {
		return new Circle(this);
	}

	@Override
	public String toString() {
		return String.format("Tâm (%d, %d) , bán kính %d", this.getX(), this.getY(), this.getBanKinh());
	}

}
