package models;

import services.NguoiQuanLy;
import services.YeuCau;

public class TruongBoPhan extends NguoiQuanLy {

    @Override
    protected boolean coTheGiaiQuyet(int numberOfDays) {
	return numberOfDays <= 5;
    }

    @Override
    protected void giaiQuyet(YeuCau yc) {
	System.out.println("Truong bo phan giai quyet yeu cau xin nghi " + yc.getDays() + " ngay!");
    }
}
