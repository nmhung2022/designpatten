package models;

import services.NguoiQuanLy;
import services.YeuCau;

public class ToTruong extends NguoiQuanLy {

    @Override
    protected boolean coTheGiaiQuyet(int soNgayNghi) {
	return soNgayNghi <= 3;
    }

    @Override
    protected void giaiQuyet(YeuCau yc) {
	System.out.println("Giai quyet yeu cau xin nghi " + yc.getDays() + " ngay boi Tổ trưởng");
    }

}
