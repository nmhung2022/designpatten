package services;

public abstract class NguoiQuanLy {
    protected NguoiQuanLy nextApprover;

    public void xemXetGiaiQuyet(YeuCau yc) {
	System.out.println("Người đang xử lý: " + this.getClass().getSimpleName());
	if (this.coTheGiaiQuyet(yc.getDays())) {
	    System.out.println("Đã giải quyết yêu cầu bởi " + this.getClass().getSimpleName());
	    this.giaiQuyet(yc);
	} else if (nextApprover != null) {
	    System.out.println("Người tiếp theo có thể xử lý: " + nextApprover.getClass().getSimpleName());
	    nextApprover.xemXetGiaiQuyet(yc);
	}

    }

    public void setNext(NguoiQuanLy appover) {
	this.nextApprover = appover;
    }

    protected abstract void giaiQuyet(YeuCau yc);

    protected abstract boolean coTheGiaiQuyet(int days);

}
