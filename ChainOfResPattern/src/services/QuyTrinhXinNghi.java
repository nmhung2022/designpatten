package services;

import models.GiamDoc;
import models.ToTruong;
import models.TruongBoPhan;

public class QuyTrinhXinNghi {
    public static NguoiQuanLy getNguoiQuanLy() {
	NguoiQuanLy nqlMuc1 = new ToTruong();
	NguoiQuanLy nqlMuc2 = new TruongBoPhan();
	NguoiQuanLy nqlMuc3 = new GiamDoc();

	nqlMuc1.setNext(nqlMuc2);
	nqlMuc2.setNext(nqlMuc3);
	return nqlMuc1;
    }
}
