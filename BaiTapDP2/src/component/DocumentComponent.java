package component;

public interface DocumentComponent {
    void hienThiThuocTinh();
    double tongKichThuoc();
    void setDuongDan(String GroupCha);
}
