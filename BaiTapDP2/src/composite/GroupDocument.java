package composite;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import component.DocumentComponent;
import leaf.Document;
import singleton.ConnectSingleton;

public class GroupDocument implements DocumentComponent {
    private String GUID;
    private String tenGroup;
    private LocalDate NgayTao;
    private String path;
    private List<DocumentComponent> documents;

    public GroupDocument() {
	super();
    }

    public String getGUID() {
	return GUID;
    }

    public void setGUID(String gUID) {
	GUID = gUID;
    }

    public String getTenGroup() {
	return tenGroup;
    }

    public void setTenGroup(String tenGroup) {
	this.tenGroup = tenGroup;
    }

    public LocalDate getNgayTao() {
	return NgayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
	NgayTao = ngayTao;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }

    public List<DocumentComponent> getDocuments() {
	return documents;
    }

    public void setDocuments(List<DocumentComponent> documents) {
	this.documents = documents;
    }

    @Override
    public void hienThiThuocTinh() {
	for (DocumentComponent document : documents) {
	    document.hienThiThuocTinh();
	}
    }

    @Override
    public double tongKichThuoc() {
	double total = 0;
	for (DocumentComponent document : documents) {
	    total += document.tongKichThuoc();
	}
	return total;
    }

    public GroupDocument(String GUID, String tenGroup, LocalDate ngayTao) {
	super();
	this.GUID = GUID;
	this.tenGroup = tenGroup;
	this.NgayTao = ngayTao;
	this.documents = new ArrayList<DocumentComponent>();
	path = tenGroup;
    }

    @Override
    public void setDuongDan(String GroupCha) {
	path = GroupCha + "\\" + this.getPath();
    }

    public void themDocumentVaoGroup(DocumentComponent child) {
	documents.add(child);
	child.setDuongDan(this.getPath());
    }

    public ArrayList<GroupDocument> hienThiTatCaGroup(ArrayList<GroupDocument> documents, String GUID) throws ClassNotFoundException, SQLException {
	Connection con = ConnectSingleton.getInstance().getConnection();

	Statement stmt = (Statement) con.createStatement();
	ResultSet rs = ((java.sql.Statement) stmt).executeQuery(
		"select * from TaiLieu as t join NhomTaiLieu as n on t.GUIDNhomTaiLieu = n.GUIDNhomTaiLieu where t.GUIDNhomTaiLieu = "
			+ GUID);
	DocumentComponent d = null;
	GroupDocument gd = null;
	


	while (rs.next()) {
	    String id = rs.getString("GUIDNhomTaiLieu");
	    String tenNhom = rs.getString("TenNhom");
	    LocalDate date2 = LocalDate.parse(rs.getString("NgayTaoGroup"));

	    gd = new GroupDocument(id, tenNhom, date2);

	    String idd = rs.getString("GUIDTaiLieu");
	    
	    String tenDocument = rs.getString("TenTaiLieu");

	    LocalDate date1 = LocalDate.parse(rs.getString("NgayTaoDocument"));

	    Double dungLuong = Double.parseDouble(rs.getString("DungLuongDocument"));
	    d = new Document(idd, tenDocument, date1, dungLuong);
	    
	    gd.themDocumentVaoGroup(d);
	    documents.add(gd);
	}
	return documents;

    }
}
