package leaf;

import java.time.LocalDate;
import java.util.Date;

import component.DocumentComponent;

public class Document implements DocumentComponent {
    private String GUIDTaiLieu;
    private String tenTaiLieu;
    private LocalDate ngayTao;
    private double dungLuong;
    private String path;


    public Document() {
	super();
    }
    
    public Document(String gUIDTaiLieu, String tenTaiLieu, LocalDate ngayTao, double dungLuong) {
	super();
	GUIDTaiLieu = gUIDTaiLieu;
	this.tenTaiLieu = tenTaiLieu;
	this.ngayTao = ngayTao;
	this.dungLuong = dungLuong;
	this.path = tenTaiLieu;
    }

    @Override
    public void hienThiThuocTinh() {
	System.out.println("Mã tài liệu = " + GUIDTaiLieu + ", Ngày tạo = "
		+ ngayTao + " Đường Dẫn = " + path + "");

    }

    public String getGUIDTaiLieu() {
	return GUIDTaiLieu;
    }

    public void setGUIDTaiLieu(String gUIDTaiLieu) {
	GUIDTaiLieu = gUIDTaiLieu;
    }

    public String getTenTaiLieu() {
	return tenTaiLieu;
    }

    public void setTenTaiLieu(String tenTaiLieu) {
	this.tenTaiLieu = tenTaiLieu;
    }

    public LocalDate getNgayTao() {
	return ngayTao;
    }

    public void setNgayTao(LocalDate date1) {
	this.ngayTao = date1;
    }

    public double getDungLuong() {
	return dungLuong;
    }

    public void setDungLuong(double dungLuong) {
	this.dungLuong = dungLuong;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }


    @Override
    public double tongKichThuoc() {
	return dungLuong;
    }

    @Override
    public void setDuongDan(String GroupCha) {
	path = GroupCha + "\\" + this.getPath();
    }

}
