package client;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import component.DocumentComponent;
import composite.GroupDocument;

public class Client {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, ParseException {
	ArrayList<GroupDocument> documents = new ArrayList<>();
	GroupDocument d = new GroupDocument();
	
	documents =  d.hienThiTatCaGroup(documents, "1");
	var total = 0;
	for(DocumentComponent obj: documents) {
	    obj.hienThiThuocTinh();
	    total += obj.tongKichThuoc();
	}
	
	System.out.println(total);	
    }
}
