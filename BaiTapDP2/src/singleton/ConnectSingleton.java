package singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectSingleton {
    private static ConnectSingleton dbObject;
    private static Connection con;
    private static Statement stmt;

    private ConnectSingleton() {
    }

    public static ConnectSingleton getInstance() {

	// create object if it's not already created
	if (dbObject == null) {
	    dbObject = new ConnectSingleton();
	}

	// returns the singleton object
	return dbObject;
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
	if (con == null) {
	    try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String connectionUrl = "jdbc:sqlserver://localhost:1433;database=DocumentDB;user=sa;password=123";
		con = DriverManager.getConnection(connectionUrl);
		System.out.println("Connected DB");
	    } catch (SQLException ex) {
		ex.printStackTrace();
	    }
	}
	return con;
    }
}
