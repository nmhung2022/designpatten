USE [DocumentDB]
GO
/****** Object:  Table [dbo].[NhomTaiLieu]    Script Date: 01/11/2021 17:53:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomTaiLieu](
	[GUIDNhomTaiLieu] [nvarchar](50) NOT NULL,
	[TenNhom] [varchar](50) NOT NULL,
	[NgayTaoGroup] [date] NOT NULL,
	[DungLuongGroup] [float] NOT NULL,
	[GUIDNhomParent] [nvarchar](50) NULL,
 CONSTRAINT [PK_NhomTaiLieu] PRIMARY KEY CLUSTERED 
(
	[GUIDNhomTaiLieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaiLieu]    Script Date: 01/11/2021 17:53:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiLieu](
	[GUIDTaiLieu] [nvarchar](50) NOT NULL,
	[TenTaiLieu] [varchar](50) NOT NULL,
	[NgayTaoDocument] [date] NOT NULL,
	[DungLuongDocument] [float] NOT NULL,
	[GUIDNhomTaiLieu] [nvarchar](50) NULL,
 CONSTRAINT [PK_TaiLieu] PRIMARY KEY CLUSTERED 
(
	[GUIDTaiLieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[NhomTaiLieu] ([GUIDNhomTaiLieu], [TenNhom], [NgayTaoGroup], [DungLuongGroup], [GUIDNhomParent]) VALUES (N'1         ', N'BalsamiqTool', CAST(N'2021-01-11' AS Date), 1, N'1         ')
INSERT [dbo].[NhomTaiLieu] ([GUIDNhomTaiLieu], [TenNhom], [NgayTaoGroup], [DungLuongGroup], [GUIDNhomParent]) VALUES (N'2         ', N'StartUMLTool', CAST(N'2021-01-11' AS Date), 1, N'2         ')
INSERT [dbo].[TaiLieu] ([GUIDTaiLieu], [TenTaiLieu], [NgayTaoDocument], [DungLuongDocument], [GUIDNhomTaiLieu]) VALUES (N'1', N'Bansamiq_Mokup.exe', CAST(N'2021-01-11' AS Date), 1, N'1        ')
INSERT [dbo].[TaiLieu] ([GUIDTaiLieu], [TenTaiLieu], [NgayTaoDocument], [DungLuongDocument], [GUIDNhomTaiLieu]) VALUES (N'2', N'StartUML.exe', CAST(N'2021-01-11' AS Date), 1, N'1')
ALTER TABLE [dbo].[NhomTaiLieu]  WITH CHECK ADD  CONSTRAINT [FK_NhomTaiLieu_NhomTaiLieu] FOREIGN KEY([GUIDNhomParent])
REFERENCES [dbo].[NhomTaiLieu] ([GUIDNhomTaiLieu])
GO
ALTER TABLE [dbo].[NhomTaiLieu] CHECK CONSTRAINT [FK_NhomTaiLieu_NhomTaiLieu]
GO
ALTER TABLE [dbo].[TaiLieu]  WITH CHECK ADD  CONSTRAINT [FK_TaiLieu_NhomTaiLieu] FOREIGN KEY([GUIDNhomTaiLieu])
REFERENCES [dbo].[NhomTaiLieu] ([GUIDNhomTaiLieu])
GO
ALTER TABLE [dbo].[TaiLieu] CHECK CONSTRAINT [FK_TaiLieu_NhomTaiLieu]
GO
