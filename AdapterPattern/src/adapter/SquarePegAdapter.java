package adapter;

import newobject.SquarePeg;
import old.RoundPeg;

/**
 * Adapter PHẢI
 *  - "có mối liên hệ" với đối tượng mới (HAS-A)
 * 	- kế thừa tự đối tượng cũ (IS-A)
 * 		+ override lại phương thức của đối tượng cũ
 *  
 * @author nvtrung
 *
 */
public class SquarePegAdapter extends RoundPeg {
	private SquarePeg peg;

	public SquarePegAdapter(SquarePeg peg) {
		this.peg = peg;
	}

	@Override
	public double getRadius() {
		double result;
		result = (Math.sqrt(Math.pow((peg.getWidth() / 2), 2) * 2));
		return result;
	}
}
