package newobject;


import old.RoundPeg;

/**
 * Đối tượng mới Muốn nó có thể tận dụng lại được dịch vụ cũ
 * 
 * @author nvtrung
 *
 */
public class SquarePeg {
    private double width;

    public SquarePeg(double width) {
	this.width = width;
    }

    public double getWidth() {
	return width;
    }

    public double getSquare() {
	double result;
	result = Math.pow(this.width, 2);
	return result;
    }

  
}
