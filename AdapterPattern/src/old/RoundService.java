package old;

/**
 * Dịch vụ cũ, làm việc được với RoundPeg (cọc hình trụ)
 * 
 * @author nvtrung
 *
 */
public class RoundService {
	private double radius;

	public RoundService(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public boolean fits(RoundPeg peg) {
		boolean result;
		result = (this.getRadius() >= peg.getRadius());
		return result;
	}
}
