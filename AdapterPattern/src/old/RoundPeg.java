package old;

/**
 * Đối tượng cọc hình trụ
 * 
 * @author nvtrung
 *
 */
public class RoundPeg {
	private double radius;

	public RoundPeg() {
	}

	public RoundPeg(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}
}
