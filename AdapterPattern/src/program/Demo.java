package program;

import adapter.SquarePegAdapter;
import newobject.SquarePeg;
import old.RoundPeg;
import old.RoundService;

public class Demo {
	public static void main(String[] args) {
		
		// Minh họa sử dụng dịch vụ với đối tượng cũ
		RoundService service = new RoundService(5);
		RoundPeg rpeg = new RoundPeg(5);
		if (service.fits(rpeg)) {
			System.out.println("Cọc bán kính r5 chui lọt lỗ r5.");
		}

		// Minh họa sử dụng dịch vụ với đối tượng mới
		SquarePeg cocVuong2 = new SquarePeg(2);
		SquarePeg cocVuong20 = new SquarePeg(20);
		// service.fits(cocVuong2); // LỖI!!!

		// Adapter = Cọc tròn bọc ngoài cọc vuông
		SquarePegAdapter smallSqPegAdapter = new SquarePegAdapter(cocVuong2);
		SquarePegAdapter largeSqPegAdapter = new SquarePegAdapter(cocVuong20);
		if (service.fits(smallSqPegAdapter)) {
			System.out.println("Cọc kích thước w2 chui lọt r5.");
		}
		if (!service.fits(largeSqPegAdapter)) {
			System.out.println("Cọc kích thước w20 không chui lọt r5.");
		}
	}
}
