package providers;

import java.util.ArrayList;
import java.util.List;

import consumers.INewsConsumer;

public class NewsProducer {
	private String news;

	private List<INewsConsumer> lstConsumer = new ArrayList<>();

	public void addConsumer(INewsConsumer consumer) {
		this.lstConsumer.add(consumer);
	}

	public void removeConsumer(INewsConsumer consumer) {
		this.lstConsumer.remove(consumer);
	}

	public void setNews(String news) {
		this.news = news;
		
		for (INewsConsumer consumer : this.lstConsumer) {
			System.out.println("Báo để " + consumer.getID() + " xử lý cập nhật thông tin [" + news + "]");
			consumer.onNewsChanged(this.news);
		}
	}
}
