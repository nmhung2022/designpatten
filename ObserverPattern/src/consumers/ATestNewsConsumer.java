package consumers;

public class ATestNewsConsumer implements INewsConsumer {
    private String name;

    private String myNews;

    @Override
    public String getID() {
	return this.getClass().getSimpleName() + "." + this.name;
    }

    public ATestNewsConsumer(String name) {
	this.name = name;
    }

    public void setMyNews(String value) {
	this.myNews = value;
    }

    public String getMyNews() {
	return this.myNews;
    }

    @Override
    public void onNewsChanged(Object oNews) {
	String newNews = (String) oNews;
	this.setMyNews(newNews.toUpperCase());
    }

}
