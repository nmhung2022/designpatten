package consumers;

public class AnotherNewsConsumer implements INewsConsumer {
	private String tinTuc;

	@Override
	public void onNewsChanged(Object oNews) {
		if (oNews == null)
			this.tinTuc = "?";
		else {
			String tinGoc = (String) oNews;
			this.tinTuc = tinGoc.toLowerCase();
		}
	}

	public String getTinTuc() {
		return this.tinTuc;
	}

	@Override
	public String getID() {
		return this.getClass().getSimpleName() + "." + this.hashCode();
	}

}
