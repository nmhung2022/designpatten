package consumers;

public interface INewsConsumer {
	// Phương thức xử lý tin mới nhận được
	public void onNewsChanged(Object oNews);
	
	// Phương thức cho biết định danh của đối tượng
	public String getID();
}
