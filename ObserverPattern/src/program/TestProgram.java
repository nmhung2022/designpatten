package program;

import consumers.ATestNewsConsumer;
import consumers.AnotherNewsConsumer;
import providers.NewsProducer;

public class TestProgram {

	public static void main(String[] args) {
		NewsProducer subject = new NewsProducer();
		
		ATestNewsConsumer observer = new ATestNewsConsumer("Tuổi Trẻ");
		ATestNewsConsumer observer2 = new ATestNewsConsumer("VnExpress");
		AnotherNewsConsumer observer3 = new AnotherNewsConsumer();

		subject.addConsumer(observer);
		subject.addConsumer(observer2);
		subject.addConsumer(observer3);
		
		subject.setNews("This is a test news!");
//		assertEquals(observer.getNews(), "news");
		
		System.out.println("\n\n-----");
		System.out.println("Tin tuc cua " + observer.getID() + ": " + observer.getMyNews());
		System.out.println("Tin tuc cua " + observer2.getID() + ": " + observer2.getMyNews());
		System.out.println("Tin tuc cua " + observer3.getID() + ": " + observer3.getTinTuc());
	}

}
