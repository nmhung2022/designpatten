package myfacade;

import java.io.File;

import thuvien.AudioMixer;
import thuvien.BitrateReader;
import thuvien.Codec;
import thuvien.CodecFactory;
import thuvien.MPEG4CompressionCodec;
import thuvien.OggCompressionCodec;
import thuvien.VideoFile;

public class VideoConversionFacade {
	public File convertVideo(String fileName, String format) {
		System.out.println("Bắt đầu chuyển đổi");
		
		// Bước 1. Tạo đối tượng VideoFile từ filename
		System.out.println("Bước 1...");
		VideoFile file = new VideoFile(fileName);
		
		// Bước 2. Lấy codec của filename
		System.out.println("Bước 2...");
		Codec sourceCodec = CodecFactory.extract(file);
		
		// Bước 3. Lấy thông tin codec theo định dạng muốn chuyển đối
		System.out.println("Bước 3...");
		Codec destinationCodec;
		if (format.equals("mp4")) {
			destinationCodec = new OggCompressionCodec();
		} else {
			destinationCodec = new MPEG4CompressionCodec();
		}
		
		// Bước 4. Tạo buffer để đọc từng cụm của file gốc, theo codec của file gốc
		System.out.println("Bước 4...");
		VideoFile buffer = BitrateReader.read(file, sourceCodec);
		
		// Bước 5. Ghi dữ liệu chuyển đổi theo định dạng mới vào file tạm
		System.out.println("Bước 5...");
		VideoFile intermediateResult = BitrateReader.convert(buffer, destinationCodec);
		
		// Bước 6. Tổng hợp file kết quả
		System.out.println("Bước 6...");
		File result = (new AudioMixer()).fix(intermediateResult);
		System.out.println("... phùuuuuuuu, xong rồi");
		
		// Bước 7. Trả về kết quả
		return result;
	}
}
