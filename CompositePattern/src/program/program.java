package program;

import java.util.Arrays;
import java.util.List;

import model.File;
import model.Folder;
import service.Component;

public class program {
    public static void main(String[] args) {
	File file1 = new File("file 1", 10);
	File file2 = new File("file 2", 5);
	File file3 = new File("file 3", 12);
	
	File[] arr = { file1,file2, file3 };

	List<Component> files = Arrays.asList(arr);

	for (Component str : files) {
	    System.out.println(str.toString());
	}

	Component folder = new Folder("Folder 1", files);

	System.out.println("Total Size: " + folder.getSize());
    }
}
