package service;

public interface Component {
    public double getSize();
}
