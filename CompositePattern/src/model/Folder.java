package model;

import java.util.ArrayList;
import java.util.List;

import service.Component;

public class Folder implements Component {
    private List<Component> listFiles = new ArrayList<>();
    private String ten;

    public Folder(String ten, List<Component> listFiles) {
	this.listFiles = listFiles;
	this.ten = ten;
    }

    @Override
    public double getSize() {
	double tong = 0;
	for (Component file : listFiles) {
	    tong += file.getSize();
	}
	return tong;
    }

}
