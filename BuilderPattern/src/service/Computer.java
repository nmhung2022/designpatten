package service;

public class Computer {
    private int dungLuongRAM; // required
    private int dungLuongHDD; // required
    private boolean coGPU; // optional
    private boolean coSoundCard; // optional

    private Computer(int dungLuongRAM, int dungLuongHDD, boolean coGPU, boolean coSoundCard) {
	super();
	this.dungLuongRAM = dungLuongRAM;
	this.dungLuongHDD = dungLuongHDD;
	this.coGPU = coGPU;
	this.coSoundCard = coSoundCard;
    }

    private Computer(ComputerBuilder builder) {
	this.dungLuongHDD = builder.dungLuongHDD;
	this.dungLuongRAM = builder.dungLuongRAM;
	this.coGPU = builder.coGPU;
	this.coSoundCard = builder.coSoundCard;
    }

    public boolean getCoGPU() {
	return coGPU;
    }

    public void setCoGPU(boolean coGPU) {
	this.coGPU = coGPU;
    }

    public boolean getCoSoundCard() {
	return coSoundCard;
    }

    public void setCoSoundCard(boolean coSoundCard) {
	this.coSoundCard = coSoundCard;
    }

    public int getDungLuongRAM() {
	return dungLuongRAM;
    }

    public void setDungLuongRAM(int dungLuongRAM) {
	this.dungLuongRAM = dungLuongRAM;
    }

    public int getDungLuongHDD() {
	return dungLuongHDD;
    }

    public void setDungLuongHDD(int dungLuongHDD) {
	this.dungLuongHDD = dungLuongHDD;
    }

    @Override
    public String toString() {
	return String.format("Ram: %dGB, HDD: %dGB, có GPU: %b, có Sound Card: %b", this.getDungLuongRAM(),
		this.getDungLuongHDD(), this.getCoGPU(), this.getCoSoundCard());
    }

    // Class builder
    public static class ComputerBuilder {
	private int dungLuongRAM; // required
	private int dungLuongHDD; // required
	private boolean coGPU; // optional
	private boolean coSoundCard; // optional

	public ComputerBuilder(int ram, int hdd) {
	    this.dungLuongHDD = hdd;
	    this.dungLuongRAM = ram;
	}

	public ComputerBuilder setCoGPU(boolean value) {
	    this.coGPU = value;
	    return this;
	}

	public ComputerBuilder setSoundCard(boolean value) {
	    this.coSoundCard = value;
	    return this;
	}

	public Computer build() {
	    return new Computer(this);
	}

    }
}