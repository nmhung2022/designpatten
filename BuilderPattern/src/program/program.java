package program;

import service.Computer;
import service.Computer.ComputerBuilder;;

public class program {

    public static void main(String[] args) {
	// TODO Auto-generated method stub
	Computer x;
	x = new ComputerBuilder(1, 2).build();
	System.out.println(x.toString());
	x = new ComputerBuilder(1, 2).setCoGPU(true).setSoundCard(true).build();
	System.out.println(x.toString());
    }

}
